jQuery(function($) {
    $('a.media-link').click(function() {
        jwplayer("player").setup({
            file: $(this).attr('href'),
            autostart: true
        }).setFullscreen(true);
        return false;
    });
});