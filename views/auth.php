<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Test task</title>
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="./css/style.css" rel="stylesheet" media="screen">
</head>
<body>
<div id="content">
    <?php
    //Иниацилизируем класс
    $api = new $config['classApi']($config['api']);
    //Засекаем время для токена
    $time = time();
    if (isset($_POST['user']) && isset($_POST['password']) && $token = $api->auth($_POST['user'], $_POST['password']))
    {
        //Записываем токен и время в сессию
        $_SESSION['token'] = $token;
        $_SESSION['time'] = $time;

        //Делаем редирект на глаавную страницу дабы избежать повторной отправки формы по F5
        header("Location: /utk/index.php");
    }
    ?>
    <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Авторизуйтесь, пожалуйста.</h2>
        <input type="text" name="user" class="input-block-level" placeholder="Логин">
        <input type="password" name="password" class="input-block-level" placeholder="Пароль">
        <button id="btn-login" class="btn btn-large btn-primary" type="submit">Войти</button>
    </form>
</div>
<!-- /content -->
</body>
</html>

