<?php
/**
 * @var $api AbstractApi
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Test task</title>
    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="./css/style.css" rel="stylesheet" media="screen">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script type="text/javascript" src="./player/jwplayer.js"></script>
    <script type="text/javascript" src="./player/player.js"></script>
</head>
<body>
<div id="content">
    <?php
    //Иниацилизируем класс
    $api = new $config['classApi']($config['api']);

    //Извлекаем токен из сессии
    $token = $api->auth('test', 'test');

    //Засекаем время обновления токена
    $time = time();

    //Пытаемся получить список медиа
    $media = $api->getMedia($token);

    if ($media['error'] == '403')
    {
        //Возможно наш токен уже умер, попробуем заново авторизоваться
        unset($_SESSION['token']);
        unset($_SESSION['time']);
        header("Location: /utk/index.php");
    }

    //Обновляем время создания токена
    $_SESSION['time'] = $time;

    ?>
    <div id="player"></div>
    <ul class="media-list">
        <?php foreach ($media['list'] as $m) : ?>
            <li>
                <a class="media-link" href="<?php echo $m['aurl']; ?>"
                   title="Жанр: <?php echo $m['genre']; ?>">
                    <?php echo $m['artist'] . ' - ' . $m['title']; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div><!-- /content -->
</body>
</html>

