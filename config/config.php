<?php
return array(
    //Класс работы с api (CurlApi или FileGetContentsApi)
    'classApi' => 'CurlApi',
    //Настройки api-сервера
    'api' => array(
        'url' => 'http://vacancy.dev.telehouse-ua.net',
        'auth' => '/auth/login',
        'media' => '/media/list',
        'liveSessionTime' => 20
    )
);
