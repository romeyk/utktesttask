<?php
/**
 *
 * @var  $config
 */

//Получаем массив конфига
$config = require dirname(__FILE__) . '/config/config.php';

//Подключаем класс для работы с api
require dirname(__FILE__) . '/lib/' . $config['classApi'] . '.php';

//Проверяем наличие токена в сессии и не истекло ли время жизни токена на сервере
session_start();
if (isset($_SESSION['token']) && isset($_SESSION['time']) && time() - $_SESSION['time'] < $config['api']['liveSessionTime'] * 60)
    require dirname(__FILE__) . '/views/media.php';
else
    require dirname(__FILE__) . '/views/auth.php';


