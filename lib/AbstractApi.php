<?php
/**
 * Базовый класс для работы с api
 */
abstract class AbstractApi
{
    protected $_url;
    protected $_auth;
    protected $_media;
    protected $_liveSessionTime;

    /**
     * Конструктор класса
     * @param array $config параметры для работы с api
     */
    public function __construct($config)
    {
        $this->_url = $config['url'];
        $this->_auth = $config['auth'];
        $this->_media = $config['media'];
        $this->_liveSessionTime = $config['liveSessionTime'];
    }

    /**
     * Функция для отсылки запроса api-серверу
     * @param string $url адрес по которому будет обращение
     * @param mixed $header заголовок запроса к серверу
     */
    abstract public function send($url, $header = null);

    /**
     * Функция авторизации на сервере
     * @param string $user
     * @param string $password
     * @throws Exception
     * @return mixed
     */
    public function auth($user, $password)
    {
        $url = $this->_url . $this->_auth . '?login=' . $user . '&password=' . $password;
        $result = $this->send($url);
        if (isset($result['error']) && $result['error'] == '403') throw new Exception('Неправильный логин или пароль.');
        elseif (isset($result['token'])) return $result['token'];
        else throw new Exception('Ошибка сервера.');
    }

    /**
     * Функция для получения списка медиа
     * @param string $token
     * @return mixed
     * @throws Exception
     */
    public function getMedia($token)
    {
        $url = $this->_url . $this->_media;
        $header = "X-Auth-Token: " . $token;
        $result = $this->send($url, $header);
        if (isset($result['error']) && (($result['error'] === 0 && isset($result['list'])) || $result['error'] == '403'))
            return $result;
        else throw new Exception('Ошибка сервера.');
    }
}