<?php
/**
 * Класс для работы с api через file_get_contents
 */

require_once dirname(__FILE__) . '/AbstractApi.php';

class FileGetContentsApi extends AbstractApi
{
    /**
     * Функция для отсылки запроса api-серверу
     * @param string $url адрес по которому будет обращение
     * @param mixed $header заголовок запроса к серверу
     * @return mixed
     */
    public function send($url, $header = null)
    {
        if ($header !== null)
        {
            $opts = array(
                'http' => array(
                    'method' => "GET",
                    'header' => $header
                )
            );
            $context = stream_context_create($opts);
            $result = file_get_contents($url, false, $context);
        }
        else $result = file_get_contents($url);

        if ($result)
            return json_decode($result, true);
        elseif ($result === false && isset($http_response_header[0]))
            //Если ошибка - отдаем ее код
            return array('error' => trim(str_replace('HTTP/1.1', '', $http_response_header[0])));
        else return false;
    }
}