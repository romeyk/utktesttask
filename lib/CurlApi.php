<?php

require_once dirname(__FILE__) . '/AbstractApi.php';

class CurlApi extends AbstractApi
{
    public function send($url, $header = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if ($header !== null) curl_setopt($curl, CURLOPT_HTTPHEADER, array($header));
        $result = curl_exec($curl);

        $responseHeader = curl_getinfo($curl);
        curl_close($curl);

        if ($responseHeader['http_code'] == 200)
            return json_decode($result, true);
        else
            return array('error' => $responseHeader['http_code']);
    }
}